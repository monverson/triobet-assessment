package com.triobet.assessment.test;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.triobet.assessment.controller.NoteController;
import com.triobet.assessment.entity.Note;
import com.triobet.assessment.repository.NoteRepository;

@WebMvcTest(NoteController.class)
@RunWith(SpringRunner.class)
@EnableSpringDataWebSupport
public class NoteControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private NoteRepository noteRepository;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(new NoteController()).build();
	}

	@Test
	public void addNotes() throws Exception {
		this.mockMvc.perform(get("/notes/addnote")).andExpect(status().isOk());
	}

	@SuppressWarnings("deprecation")
	@Test
	public void showUpdateForm() throws Exception {

		MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build()
				.perform(MockMvcRequestBuilders.get("/notes/addnote").accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk());

	}

	@Test
	public void addNote() throws Exception {
		Note note = new Note();
		note.setId(1L);
		note.setName("TEST");
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = format.parse("2019-10-21");
		note.setDate(date);
		noteRepository.save(note);
		when(noteRepository.findById(1L)).thenReturn(Optional.of(note));
		assertTrue(noteRepository.findById(1L).get().getName().equals("TEST"));

	}

}
