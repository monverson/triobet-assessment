package com.triobet.assessment.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.triobet.assessment.entity.Note;

@Repository
public interface NoteRepository extends PagingAndSortingRepository<Note, Long> {

}
