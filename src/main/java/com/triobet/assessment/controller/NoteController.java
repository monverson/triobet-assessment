package com.triobet.assessment.controller;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.triobet.assessment.entity.Note;
import com.triobet.assessment.repository.NoteRepository;

@Controller
@RequestMapping("/notes/")
public class NoteController {

	@Autowired
	private NoteRepository noteRepository;

	public NoteController() {
		super();
	}

	@GetMapping("addnote")
	public String showSignUpForm(Note note) {
		return "add-note";
	}

	@GetMapping("listnotes")
	public String showUpdateForm(@PageableDefault(size = 10, sort = "date") Pageable pageable, Model model) {
		Page<Note> page = noteRepository.findAll(pageable);

		List<Sort.Order> sortOrders = page.getSort().stream().collect(Collectors.toList());

		if (sortOrders.size() > 0) {
			Sort.Order order = sortOrders.get(0);
			model.addAttribute("sortProperty", order.getProperty());
			model.addAttribute("sortDesc", order.getDirection() == Sort.Direction.DESC);
		}

		model.addAttribute("notes", page);
		return "index";
	}

	@PostMapping("add")
	public String addNote(@Valid Note note, BindingResult result) {
		if (result.hasErrors()) {
			return "add-note";
		}

		note.setDate(new Date());
		noteRepository.save(note);
		return "redirect:listnotes";
	}

}
